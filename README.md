## jQuery TicTacToe Game ##
originally built following along with a 2014 tutorial on Udemy

as practice, I am trying to improve the code.

### Changelog ###
17 Jan 2019:

- updated file structure, paths, and cdn for jquery
- attempted to shorted code for creating position variables by looping through an array
- realized the array wasn't what I was looking for, went back to simple assignment
- created function for resetBoard
- defined winning conditions
- created function to check for winning conditions
- make sure winning condition was cleared at end of game


### Future Plans ###
These are the ideas I've had on changing/updating/improving this.

- allow user to choose X or O for starting turn
- mobile compatibility (touch, overlay instead of alert, etc)







