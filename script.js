var x = "x";
var o = "o";
var turns = 0;

// winning row variables
var hr1o, hr2o, hr3o, vr1o, vr2o, vr3o, dr1o, dr2o = false;
var hr1x, hr2x, hr3x, vr1x, vr2x, vr3x, dr1x, dr2x = false;

// reset handler
function resetBoard(){
	winningRow = '';
	$("#board li").text("+");
	$("#board li").removeClass('disable x o');
	turns = 0;
}


$(document).ready(function(){
	// assign position variables
	var pos1 = $('#pos1');
	var pos2 = $('#pos2');
	var pos3 = $('#pos3');
	var pos4 = $('#pos4');
	var pos5 = $('#pos5');
	var pos6 = $('#pos6');
	var pos7 = $('#pos7');
	var pos8 = $('#pos8');
	var pos9 = $('#pos9');

	// Reset Handler
	$("#reset").on('click', resetBoard);

	//function to check for wins
	function check4win() {
		console.log('checking for win...');
		var winningRow;
		// define conditions for a win
		if (pos1.hasClass('o') && pos2.hasClass('o') && pos3.hasClass('o')) { hr1o = true; winningRow = 'hr1o'; }
		if (pos4.hasClass('o') && pos5.hasClass('o') && pos6.hasClass('o')) { hr2o = true; winningRow = 'hr2o'; }
		if (pos7.hasClass('o') && pos8.hasClass('o') && pos9.hasClass('o')) { hr3o = true; winningRow = 'hr3o'; }
		if (pos1.hasClass('o') && pos5.hasClass('o') && pos9.hasClass('o')) { dr1o = true; winningRow = 'dr1o'; }
		if (pos3.hasClass('o') && pos5.hasClass('o') && pos7.hasClass('o')) { dr2o = true; winningRow = 'dr2o'; }
		if (pos1.hasClass('o') && pos4.hasClass('o') && pos7.hasClass('o')) { vr1o = true; winningRow = 'vr1o'; }
		if (pos2.hasClass('o') && pos5.hasClass('o') && pos8.hasClass('o')) { vr2o = true; winningRow = 'vr2o'; }
		if (pos3.hasClass('o') && pos6.hasClass('o') && pos9.hasClass('o')) { vr3o = true; winningRow = 'vr3o'; }
		
		if (pos1.hasClass('x') && pos2.hasClass('x') && pos3.hasClass('x')) { hr1x = true; winningRow = 'hr1x'; }
		if (pos4.hasClass('x') && pos5.hasClass('x') && pos6.hasClass('x')) { hr2x = true; winningRow = 'hr2x'; }
		if (pos7.hasClass('x') && pos8.hasClass('x') && pos9.hasClass('x')) { hr3x = true; winningRow = 'hr3x'; }
		if (pos1.hasClass('x') && pos4.hasClass('x') && pos7.hasClass('x')) { vr1x = true; winningRow = 'vr1x'; }
		if (pos2.hasClass('x') && pos5.hasClass('x') && pos8.hasClass('x')) { vr2x = true; winningRow = 'vr2x'; }
		if (pos3.hasClass('x') && pos6.hasClass('x') && pos9.hasClass('x')) { vr3x = true; winningRow = 'vr3x'; }
		if (pos1.hasClass('x') && pos5.hasClass('x') && pos9.hasClass('x')) { dr1x = true; winningRow = 'dr1x'; }
		if (pos3.hasClass('x') && pos5.hasClass('x') && pos7.hasClass('x')) { dr2x = true; winningRow = 'dr2x'; }

		// were any win conditions set?
		if (hr1o || hr2o || hr3o || vr1o || vr2o || vr3o || dr1o || dr2o) {
			alert("Player O wins!");
			console.log("winning row was " + winningRow);
			hr1o = hr2o = hr3o = vr1o = vr2o = vr3o = dr1o = dr2o = false; // clear winning row
		} else if (hr1x || hr2x || hr3x || vr1x || vr2x || vr3x || dr1x || dr2x) {
			alert("Player X wins!");
			console.log("winning row was " + winningRow);
			hr1x = hr2x = hr3x = vr1x = vr2x = vr3x = dr1x = dr2x = false; // clear winning row
		} else if (turns == 9) {
			alert("Tie game - no winner");
		} else {
			console.log("no winner yet, returning to click listener");
			return // break out of function if game is still going
		}

		resetBoard();
	}

	// let's play!
	$('#board li').on('click', function(){
		// check if space is open
		if ($(this).hasClass('disable')){
			alert('This spot has already been claimed');
		} else if (turns%2 == 0){ // for now, O goes on even turns
			turns++;
			$(this).text(o);
			$(this).addClass('disable o');
		} else {
			turns++;
			$(this).text(x);
			$(this).addClass('disable x');
		}
		check4win();
	});
});